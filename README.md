# Machine learning with 7 models, served parallely using FastAPI and Celery (RabbitMQ + Redis)

* **Use case:** Insurance fraud detection
* **Machine Learning Models:** Support Vector Machine, Decision Tree Classifier, K-Nearest Neighbors, Random Forest Classifier, Gradient Boosting Classifier, AdaBoost Classifier, XgBoost Classifier
* **Jupyter notebook (also available in repo):** https://www.kaggle.com/code/sijils/auto-insurance-fraud-detection/notebook

## Architecture:

![Production server](/assets/ML_Production_Architecture.drawio.png)

## Run instructions:

If running outside of GitPod, run the below steps to setup the application:

1. pip install -r requirements.txt
2. docker-compose pull
3. alias redis-cli="docker exec -it redis-stack redis-cli" 
4. echo "Use redis-cli to interact with Redis here."
5. docker-compose up -d
6. gp sync-done finished

If you are running from within Gitpod, you can ignore these steps as `.gitpod.yml` will take care of running these steps. 

These steps will ensure FastAPI is up ready to serve the 7 models and also Celery up with RabbitMQ as broker and Redis as backend.

To run the whole application, follow the below steps:

* Open a terminal and navigate to `servers`. Run `python3 api_server/app.py` to start the FastAPI server
* Open a second terminal and there also navigate to `servers`. Run `celery -A celery_task_app.worker worker --loglevel=info` to start the celery application.

To login to the rabbitmq management console, default username password is guest/guest.

The FastAPI server will be up at port 8000. The swagger endpoint for the api server can be accessed by navigating to the `/docs` url of the server.