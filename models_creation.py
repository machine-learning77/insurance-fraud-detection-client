import os.path
import shutil
import pandas as pd
import numpy as np
import wget
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import missingno as msno
import pickle

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from imblearn.over_sampling import SMOTE
from sklearn import metrics
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report

from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier

import warnings

def create_models(standalone=False):
    warnings.filterwarnings('ignore')

    plt.style.use('ggplot')

    OUTPUT_DIR = './' if standalone == True else '../'

    if os.path.exists(OUTPUT_DIR + 'data') :
        shutil.rmtree(OUTPUT_DIR + 'data')

    if os.path.exists(OUTPUT_DIR + 'models') :
        shutil.rmtree(OUTPUT_DIR + 'models')

    os.makedirs(OUTPUT_DIR + 'data')
    os.makedirs(OUTPUT_DIR + 'models')

    data_url = "https://drive.google.com/uc?export=download&id=1BdCW25kcrNxPTPrO30VFnyok1pSJNs5o"
    wget.download(data_url, OUTPUT_DIR + 'data/insurance_claims.csv')  

    df=pd.read_csv(OUTPUT_DIR + 'data/insurance_claims.csv')

    df.replace('?',np.nan,inplace=True)
    df['collision_type']=df['collision_type'].fillna(df['collision_type'].mode()[0])
    df['property_damage'] = df['property_damage'].fillna(df['property_damage'].mode()[0])
    df['police_report_available'] = df['police_report_available'].fillna(df['police_report_available'].mode()[0])
    df['fraud_reported']=df['fraud_reported'].map({'Y':1,'N':0})
    to_drop=['policy_number','auto_make','auto_model','auto_year','_c39','incident_state','incident_city', 'incident_location', 'incident_date','insured_zip','policy_number','policy_bind_date','policy_state']
    df.drop(to_drop,inplace=True, axis=1)
    df.drop(columns=['age','total_claim_amount'],axis=1,inplace=True)
    X=df.drop('fraud_reported',axis=1)
    y=df['fraud_reported']
    cat_df=X.select_dtypes(include=['object'])
    cat_df=pd.get_dummies(cat_df,drop_first=True)
    num_df=X.select_dtypes(include=['int64'])
    X=pd.concat([num_df,cat_df],axis=1)
    X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.25)
    num_df = X_train[['months_as_customer', 'policy_deductable', 'umbrella_limit',
        'capital-gains', 'capital-loss', 'incident_hour_of_the_day',
        'number_of_vehicles_involved', 'bodily_injuries', 'witnesses', 'injury_claim', 'property_claim',
        'vehicle_claim']]
    scaler=StandardScaler()
    scaled_data= scaler.fit_transform(num_df)
    scaled_num_df = pd.DataFrame(data = scaled_data, columns = num_df.columns, index = X_train.index)
    X_train.drop(columns=scaled_num_df.columns,inplace=True,axis=1)
    X_train=pd.concat([X_train,scaled_num_df],axis=1)
    X_test=X_test[X_train.columns]

    smote=SMOTE()
    x_smote,y_smote=smote.fit_resample(X,y)

    print("\n")
    # SVM classification
    print("Training SVM classifier")
    svc = SVC()
    svc.fit(X_train, y_train)
    pickle.dump(svc,open(OUTPUT_DIR + 'models/SVC.pkl','wb'))

    # KNN classification
    print("Training KNN classifier")
    knn=KNeighborsClassifier(n_neighbors=30)
    knn.fit(X_train, y_train)
    pickle.dump(knn,open(OUTPUT_DIR + 'models/KNN.pkl','wb'))

    # Decision Tree classification
    print("Training Decision Tree classifier")
    dtc=DecisionTreeClassifier()
    dtc.fit(X_train,y_train)

    grid_params={
        'criterion':['gini','entropy'],
        'max_depth':[3,5,7,10],
        'min_samples_split':range(2,10,1),
        'min_samples_leaf':range(2,10,1)
    }
    grid_search=GridSearchCV(dtc,grid_params,cv=5,n_jobs=-1,verbose=0)
    grid_search.fit(X_train,y_train)

    dtc=grid_search.best_estimator_
    pickle.dump(dtc,open(OUTPUT_DIR + 'models/DTC.pkl','wb'))

    # Random Forest classification
    print("Training Random Forest classifier")
    rfc=RandomForestClassifier(criterion='entropy',max_depth=10,max_features='sqrt',min_samples_leaf= 1, min_samples_split= 3, n_estimators= 140)
    rfc.fit(X_train,y_train)
    pickle.dump(rfc,open(OUTPUT_DIR + 'models/RFC.pkl','wb'))

    # AdaBoost classification
    print("Training AdaBoost classifier")
    ada=AdaBoostClassifier(estimator=dtc)
    parameters = {
        'n_estimators' : [50, 70, 90, 120, 180, 200],
        'learning_rate' : [0.001, 0.01, 0.1, 1, 10],
        'algorithm' : ['SAMME', 'SAMME.R']
    }

    grid_search = GridSearchCV(ada, parameters, n_jobs = -1, cv = 5, verbose = 0)
    grid_search.fit(X_train, y_train)

    ada=grid_search.best_estimator_
    pickle.dump(ada,open(OUTPUT_DIR + 'models/ADA.pkl','wb'))

    # Gradient Boosting classification
    print("Training Gradient Boosting classifier")
    gbc=GradientBoostingClassifier()
    gbc.fit(X_train,y_train)
    pickle.dump(gbc,open(OUTPUT_DIR + 'models/GBC.pkl','wb'))

    # XgBoost classification
    print("Training XgBoost classifier")
    xgb=XGBClassifier()
    xgb.fit(X_train,y_train)
    pickle.dump(xgb,open(OUTPUT_DIR + 'models/XGB.pkl','wb'))

    print("All pickle files generated")

if __name__=="__main__":
    create_models(standalone=True)

