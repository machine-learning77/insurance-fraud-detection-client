import os
import sys
from celery_task_app.worker import celery_app
# from .worker import celery_app
from celery import Task
import importlib
import logging

class PredictTask(Task):
    '''
        Abstraction of Celery's Task class to support ML model loading
    '''
    def __init__(self):
        super().__init__()
        self.models = None

    def __call__(self, *args, **kwargs):
        """
        Load model on first call (i.e. first task processed)
        Avoids the need to load model on each task request
        """
        if not self.models:
            logging.info(f"Loading all models superclass...")
            sys.path.insert(0,self.path[0]+'/../')
            module_import = importlib.import_module(self.path[1])
            models_obj = getattr(module_import, self.path[2])
            self.models = models_obj()
            logging.info(f'Models superclass loaded')
        return self.run(*args, **kwargs)



@celery_app.task(
    ignore_result=False,
    bind=True,
    base=PredictTask,
    path=(os.path.dirname( __file__ ),'api_server.ml.model_classes.ml_models', 'MLModels'),
    name='{}.{}'.format(__name__, 'ML_Prediction')
    )
def predict_task(self,algorithm, data):
    result = self.models.predict(algorithm,data)
    return str(result)