import sys
from celery import Celery


# Redis totally works as broker also which is tested. 
# BROKER_URL = 'redis://localhost:6379/0'
# Using RabbitMQ as an alternative
BROKER_URL = 'amqp://guest:guest@localhost:5672/'
BACKEND_URL = 'redis://localhost:6379/0'


celery_app = Celery(
    'ML_Serve', 
    broker=BROKER_URL, 
    backend=BACKEND_URL,
    include=['celery_task_app.tasks'],
    worker_redirect_stdouts=False
    )

