import sys
import os.path
import pandas as pd
import pickle
import json

MODEL_SCRIPT_DIRECTORY = sys.path[0] +'../'
MODEL_DIRECTORY = MODEL_SCRIPT_DIRECTORY +'models/'
# MODEL_PATH = MODEL_DIRECTORY +'SVC.pkl'

class MLModels:
    def __init__(self):
        # self.model=self._load_model_from_path(MODEL_DIRECTORY + alg.upper() + '.pkl')
        self.models=self._load_models()

    @staticmethod
    def _load_models():
        models={}
        # if the models folder does not exist
        if not os.path.exists(MODEL_DIRECTORY) :
            sys.path.insert(1,MODEL_SCRIPT_DIRECTORY)
            import models_creation
            models_creation.create_models()
        # for each pickle file, load model and store it in MLModel's model variable
        for pkl_name in os.listdir(MODEL_DIRECTORY):
            pkl_path=MODEL_DIRECTORY + pkl_name
            model=pickle.load(open(pkl_path,'rb'))    
            models[pkl_name.split('.')[0]]=model    
        return models

    def predict(self,algorithm,data):
        print(f'Classifying for {algorithm}')
        data_dict=json.loads(data)
        df=pd.DataFrame.from_dict([data_dict])
        classification = self.models[algorithm.upper()].predict(df)   
        return {'algorithm': algorithm, 'classification': classification}     
        # return classification