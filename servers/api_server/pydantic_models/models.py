from pydantic import BaseModel

class Task(BaseModel):
    task_id: str
    status: str

class Prediction(BaseModel):
    task_id: str
    status: str
    result: str
'''
class InsuranceData(BaseModel):
    months_as_customer                      : int
    policy_deductable                       : int
    umbrella_limit                          : int
    capital-gains                           : int
    capital-loss                            : int
    incident_hour_of_the_day                : int
    number_of_vehicles_involved             : int
    bodily_injuries                         : int
    witnesses                               : int
    injury_claim                            : int
    property_claim                          : int
    vehicle_claim                           : int
    policy_csl_250/500                      : int
    policy_csl_500/1000                     : int
    insured_sex_MALE                        : int
    insured_education_level_College         : int
    'insured_education_level_High School'   : int
    insured_education_level_JD              : int
    insured_education_level_MD              : int
    insured_education_level_Masters         : int
    insured_education_level_PhD             : int
    insured_occupation_armed-forces         : int
    insured_occupation_craft-repair         : int
    insured_occupation_exec-managerial      : int
    insured_occupation_farming-fishing      : int
    insured_occupation_handlers-cleaners    : int
    insured_occupation_machine-op-inspct    : int
    insured_occupation_other-service        : int
    insured_occupation_priv-house-serv      : int
    insured_occupation_prof-specialty       : int
    insured_occupation_protective-serv      : int
    insured_occupation_sales                : int
    insured_occupation_tech-support         : int
    insured_occupation_transport-moving     : int
    insured_hobbies_basketball              : int
    insured_hobbies_board-games             : int
    insured_hobbies_bungie-jumping          : int
    insured_hobbies_camping                 : int
    insured_hobbies_chess                   : int
    insured_hobbies_cross-fit               : int
    insured_hobbies_dancing                 : int
    insured_hobbies_exercise                : int
    insured_hobbies_golf                    : int
    insured_hobbies_hiking                  : int
    insured_hobbies_kayaking                : int
    insured_hobbies_movies                  : int
    insured_hobbies_paintball               : int
    insured_hobbies_polo                    : int
    insured_hobbies_reading                 : int
    insured_hobbies_skydiving               : int
    insured_hobbies_sleeping                : int
    insured_hobbies_video-games             : int
    insured_hobbies_yachting                : int
    'insured_relationship_not-in-family'    : int
    insured_relationship_other-relative     : int
    insured_relationship_own-child          : int
    insured_relationship_unmarried          : int
    insured_relationship_wife               : int
    'incident_type_Parked Car'              : int
    'incident_type_Single Vehicle Collision': int
    'incident_type_Vehicle Theft'           : int
    'collision_type_Rear Collision'         : int
    'collision_type_Side Collision'         : int
    'incident_severity_Minor Damage'        : int
    'incident_severity_Total Loss'          : int
    'incident_severity_Trivial Damage'      : int
    authorities_contacted_Fire              : int
    authorities_contacted_None              : int
    authorities_contacted_Other             : int
    authorities_contacted_Police            : int
    property_damage_YES                     : int
    police_report_available_YES             : int
'''