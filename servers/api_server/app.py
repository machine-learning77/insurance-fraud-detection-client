import os
import sys

sys.path.append( os.path.join( os.path.dirname( __file__ ), '..' ) )

import uvicorn
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic_models.models import Prediction, Task
from celery.result import AsyncResult
# from model_classes.svc_model import SVCModel
from celery_task_app.tasks import predict_task

app = FastAPI()
# svc = SVCModel()


@app.get("/")
def home():
    return {"Hello":"World"}

'''
@app.post('/svc/predict/result', response_model=Prediction, status_code=200)
async def predict(insurance_data):
    classification = svc.predict(insurance_data)
    return {'task_id': 'NA', 'status': 'Completed', 'classification':str(classification)}
'''

@app.post('/{algorithm}/predict', response_model=Task, status_code=202)
async def predict(algorithm,data):
    """Create celery prediction task. Return task_id to client in order to retrieve result"""
    task_id = predict_task.delay(algorithm,data)
    return {'task_id': str(task_id), 'status': 'Processing'}

@app.get('/predict/result/{task_id}', response_model=Prediction, status_code=200,
         responses={202: {'model': Task, 'description': 'Accepted: Not Ready'}})
async def prediction_result(task_id):
    """Fetch result for given task_id"""
    task = AsyncResult(task_id)
    if not task.ready():
        return JSONResponse(status_code=202, content={'task_id': str(task_id), 'status': 'Processing'})
    result = task.get()
    return {'task_id': task_id, 'status': 'Success', 'result': str(result)}


if __name__=="__main__":
    uvicorn.run(app)